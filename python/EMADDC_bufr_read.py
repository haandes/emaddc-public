import sys
import os
import eccodes
import numpy as np
import pandas as pd
import logging
import gzip
import shutil
def EMADDC_bufr_read(filename, convert_id=False, reduce_dtypes=False):
    """
    Function to read EMADDC bufr files containing subsets
    input:
    filename      : name of BUFR file to read
    convert_id    : boolean to convert aircraft id from hex to decimal or vice-versa
    reduce_dtypes : boolean to convert data to float32 and/or categoricals

    returns a pandas dataframe with read values from EMADDC bufr file
    """
    # Check if file is compressed uzing gzip, so check extension
    _, extension = os.path.splitext(filename)

    def read_array_from_key(bufr, key, number_of_obs):
        array = None
        try:
            array = eccodes.codes_get_array(bufr, key)
            if len(array) == 1:
                array = np.repeat(array, number_of_obs)
        except Exception:
            logging.exception(f"Could not read from {key} from file")
        return array

    # For now, uncompress first since ecCodes does not appreciate a gzip.open object
    # see https://jira.ecmwf.int/projects/SUP/issues/SUP-3537?filter=allopenissues
    # Remove this part (up to opener) once ecCodes can properly handle this
    outfile_path = None
    if "gz" in extension:
        dirpath = os.path.dirname(os.path.abspath(filename))
        basename, extension = os.path.splitext(os.path.abspath(filename))
        archive_path = os.path.join(dirpath, basename + extension)
        outfile_path = os.path.join(dirpath, f"{basename}.tmp")
        with gzip.open(archive_path, "rb") as src_file, open(outfile_path, "wb") as dest_file:
            shutil.copyfileobj(src_file, dest_file)
        filename = outfile_path
        _, extension = os.path.splitext(filename)

    # Check which file_handler to used based on extension
    opener = gzip.open if "gz" in extension else open
    count = 0
    df_list = []
    with opener(filename, "rb") as file_handle:
        # loop for the messages in the file
        while 1:
            # get handle for message
            ibufr = eccodes.codes_bufr_new_from_file(file_handle)
            if ibufr is None:
                break

            logging.debug(f"**************** MESSAGE: {count + 1} *****************")

            # we need from instruct ecCodes from expand all the descriptors
            # i.e. unpack the data values
            eccodes.codes_set(ibufr, "unpack", 1)
            # Get number of obs in this message
            number_of_obs = eccodes.codes_get(ibufr, "numberOfSubsets")
            # Get dataSubCategory
            data_sub_category = eccodes.codes_get(ibufr, "dataSubCategory")
            logging.debug(f"dataSubCategory: {data_sub_category} and number of obs: {number_of_obs}")
            keys = [
                "aircraftRegistrationNumberOrOtherIdentification",
                "icaoRegionIdentifier",
                "stationOrSiteName",
                "featureName",
                "softwareVersionNumber",
                "year",
                "month",
                "day",
                "hour",
                "minute",
                "second",
                "latitude",
                "longitude",
                "flightLevel",
                "detailedPhaseOfFlight",
                "windDirection",
                "windSpeed",
                "windSpeed->associatedField",
                "aircraftRollAngleQuality",
                "airTemperature",
                "airTemperature->associatedField",
                "qualityInformation",
            ]
            bufr_data_read = {key: read_array_from_key(ibufr, key, number_of_obs) for key in keys}
            logging.debug(
                "Date and time: {:02d}/{:02d}/{:02d} {:02d}:{:02d}:{:02d}".format(
                    bufr_data_read["year"][0],
                    bufr_data_read["month"][0],
                    bufr_data_read["day"][0],
                    bufr_data_read["hour"][0],
                    bufr_data_read["minute"][0],
                    bufr_data_read["second"][0],
                )
            )
            # Conversions
            bufr_data_read["flightLevel"] = bufr_data_read["flightLevel"] / 30.48
            bufr_data_read["windSpeed"] = np.where(
                bufr_data_read["windSpeed"] < 0, np.nan, bufr_data_read["windSpeed"]
            )  # Descriptor 011002 is in m/s
            bufr_data_read["windDirection"] = np.where(
                (bufr_data_read["windDirection"] < 0) | (bufr_data_read["windDirection"] > 360), np.nan, bufr_data_read["windDirection"]
            )
            bufr_data_read["airTemperature"] = np.where(bufr_data_read["airTemperature"] < 0, np.nan, bufr_data_read["airTemperature"])
            bufr_data_read["wl_flag"] = np.where(
                (bufr_data_read["windSpeed->associatedField"] == 1) & (bufr_data_read["airTemperature->associatedField"] == 1),
                3,
                np.where(
                    (bufr_data_read["windSpeed->associatedField"] == 1),
                    1,
                    np.where((bufr_data_read["airTemperature->associatedField"] == 1), 2, 0),
                ),
            )

            # Create df out of the read data
            df_part = pd.DataFrame(
                {
                    "Mid": bufr_data_read["aircraftRegistrationNumberOrOtherIdentification"],
                    "year": bufr_data_read["year"],
                    "month": bufr_data_read["month"],
                    "day": bufr_data_read["day"],
                    "hour": bufr_data_read["hour"],
                    "minute": bufr_data_read["minute"],
                    "second": bufr_data_read["second"],
                    "lat": bufr_data_read["latitude"],
                    "lon": bufr_data_read["longitude"],
                    "fl": bufr_data_read["flightLevel"],
                    "wspd": bufr_data_read["windSpeed"],
                    "wdir": bufr_data_read["windDirection"],
                    "temp": bufr_data_read["airTemperature"],
                    "phase": bufr_data_read["detailedPhaseOfFlight"],
                    "ra_qc": bufr_data_read["aircraftRollAngleQuality"],
                    "source": bufr_data_read["icaoRegionIdentifier"],
                    "sic": bufr_data_read["stationOrSiteName"],
                    "wl_flag": bufr_data_read["wl_flag"],
                    "qc_flag": bufr_data_read["qualityInformation"],
                }
            )
            # Add df to list
            df_list.append(df_part)
            count += 1

            # Release the BUFR message
            eccodes.codes_release(ibufr)

    # TODO: remove this part if no longer needed
    # Remove temporary uncompressed file
    if outfile_path:
        os.remove(outfile_path)

    # Check that we have retrieved data
    if not df_list:
        logging.error(f"No data retrieved from {filename}")
        return pd.DataFrame()

    # Combine all df_parts into df
    df_out = pd.concat(df_list, axis=0)
    # Create dtg
    df_out.loc[:, "dtg"] = pd.to_datetime(df_out[["year", "month", "day", "hour", "minute", "second"]])
    # df.loc[:, 'dtg'] = df.dtg.dt.tz_localize(dt.timezone.utc)
    # Rename columns if MRAR data
    if data_sub_category == 148:
        df_out = df_out.rename(columns={"wspd": "wspd_44", "wdir": "wdir_44", "temp": "t_44"})
    # Get id's if needed
    if convert_id:
        try:
            df_out.loc[:, "id"] = df_out[["Mid"]].groupby("Mid").Mid.transform(lambda a: int(emINV.Mid2ICAOC(a), 16))
        except Exception:
            logging.exception("Could not parse Mid to icao id:")
            df_out.loc[:, "id"] = df_out.Mid
    else:
        df_out.loc[:, "id"] = df_out.Mid
    # Remove unwanted columns
    df_out = df_out.drop(["year", "month", "day", "hour", "minute", "second", "Mid"], axis=1)
    # Reduce dtypes if needed
    if reduce_dtypes:
        df_out = emDB.reduceDtypes(df_out)
    # Set order
    cols = df_out.columns.to_list()
    cols = [elem for elem in cols if elem not in ["id", "dtg"]]
    cols.insert(0, "id")
    cols.insert(0, "dtg")
    df_out = df_out[cols].reset_index(drop=True)
    return df_out

if __name__ == "__main__":
    try:
        df=EMADDC_bufr_read(sys.argv[1])
        print(df.head())
    except:
        print("Prints the first lines of pandas EMADDC bufr-file\n")
        print(f"Usage {sys.argv[0]} <bufr-file>")