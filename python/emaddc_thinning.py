# %%
"""
This tool reads EMADDC bufr files and thins out the data in boxes defined by a width and a height. 
The values of width and height are set at command line. The boxes are constructed by the length of the longitude arc at the equator.
and a spherical earth representation with a radius of 6378 km.

''' 20231006 - David Schonach, david.schonach@fmi.fi
Based on Siebren de Haan's, Maria Diez and Alberto Jimenez work regarding Mode-S EMADDCC thinning. 
Reads in Mode-S bufr file (tested with Mode-S_EMADDC_KNMI_oper_$DTG.bufr), filters and thins data by box_width and box_heights.

IMPORTANT: need to give nMsgs to the script
EXAMPLE in scr/Prepare_ob:

Options:
- `-f,--infile <filename>` : set the file for reading 
- `-o,--outfile <filename>` : output file name
- `--box_width` : sets the box width in km (default) 100
- `--box_height` : sets the box height 
    implemented options are:
    - `<H>ft` : sets box_height to H feet (default H=300ft approximately 100m)
    multiple values may be set by comma seperation; the height of the boxes are defined from surface to top
    examples:
    `--box_height 100ft` : defines equal height boxes of 100ft
    `--box_height 100,100,150,300ft : defines the lowest 2 boxes to be 100ft,the 3rd 150ft, and the rest 300ft

"""
import os
import datetime as dt
import pandas as pd
import numpy as np
import sys
import glob
import gzip
import argparse
from eccodes import *
realkeys = [
	'year', 'month', 'day', 'hour', 'minute', 'second',
	'latitude', 'longitude', 'flightLevel', 'detailedPhaseOfFlight',
	'windDirection','windDirection->associatedField', 'windSpeed','windSpeed->associatedField',
	'airTemperature', 'airTemperature->associatedField', 'qualityInformation'
]

stringkeys = [
	'aircraftRegistrationNumberOrOtherIdentification',
	'softwareVersionNumber', 'featureName', 'icaoRegionIdentifier','stationOrSiteName'
]

def read_emaddc_bufr(infile,n_messages):

	n_realkeys = len(realkeys)
	n_stringkeys = len(stringkeys)
	total_array_length = 0
	with open(infile, "rb") as f:
		while True:
			bufr = codes_bufr_new_from_file(f)
			if bufr is None:
				break
			codes_set(bufr, "unpack", 1)
			total_array_length += codes_get_size(bufr, "latitude")
			codes_release(bufr)
	# Initialize data arrays with NaN values
	data_array = np.full((total_array_length, n_realkeys), np.nan)
	string_data_array = np.empty((total_array_length, n_stringkeys), dtype=object)

	print("Read: ",infile)
	print(f"number of messages :{total_array_length}")	
	msg_count = 0
	start_index = 0
	with open(infile, "rb") as f:
		while True:
			bufr = codes_bufr_new_from_file(f)
			if bufr is None:
				break
			codes_set(bufr, "unpack", 1)
			array_length = codes_get_size(bufr, "latitude")
			end_index = start_index + array_length
			for key_index, key in enumerate(realkeys):
				key_size = codes_get_size(bufr, key)
				values = np.full(array_length, np.nan)
				if key_size != 0:
					if key_size == 1 and 'associatedField' not in key:
						values.fill(codes_get(bufr, key))
					else:
						values = codes_get_array(bufr, key)
				data_array[start_index:end_index, key_index] = values
			for key_index, key in enumerate(stringkeys):
				key_size = codes_get_size(bufr, key)
				string_values = np.empty(array_length, dtype=object)
				if key_size != 0:
					if key_size == 1:
						string_values.fill(codes_get_string(bufr, key))
					else:
						string_values = codes_get_array(bufr, key)
				string_data_array[start_index:end_index, key_index] = string_values

			codes_release(bufr)
			msg_count += 1
			start_index = start_index + array_length
			if n_messages is not None and msg_count >= n_messages:
				break
	return data_array,string_data_array

def create_filter_mask(data_array):
	# Define valid ranges for filtering
	valid_year = (2000, 2100)
	valid_month = (1, 12)
	valid_day = (1, 31)
	valid_hour = (0, 23)
	valid_minute = (0, 59)
	valid_second = (0, 59)
	valid_latitude = (-90, 90)
	valid_longitude = (-180, 180)
	valid_flightLevel = (-20, 500) # in FL = 100ft
	valid_windSpeed = (0, 500)  # in m/s
	valid_windDirection = (0, 360)  # in degrees
	valid_airTemperature = (150, 400)  # in Kelvin
	valid_qualityInformation = 0 # only obs with both qc wind and temperature

	# Create boolean masks for each condition
	mask_year = (data_array[:, 0] >= valid_year[0]) & (data_array[:, 0] <= valid_year[1])
	mask_month = (data_array[:, 1] >= valid_month[0]) & (data_array[:, 1] <= valid_month[1])
	mask_day = (data_array[:, 2] >= valid_day[0]) & (data_array[:, 2] <= valid_day[1])
	mask_hour = (data_array[:, 3] >= valid_hour[0]) & (data_array[:, 3] <= valid_hour[1])
	mask_minute = (data_array[:, 4] >= valid_minute[0]) & (data_array[:, 4] <= valid_minute[1])
	mask_second = (data_array[:, 5] >= valid_second[0]) & (data_array[:, 5] <= valid_second[1])
	mask_latitude = (data_array[:, 6] >= valid_latitude[0]) & (data_array[:, 6] <= valid_latitude[1])
	mask_longitude = (data_array[:, 7] >= valid_longitude[0]) & (data_array[:, 7] <= valid_longitude[1])
	mask_flightLevel = (data_array[:, 8] >= valid_flightLevel[0]) & (data_array[:, 8] <= valid_flightLevel[1])
	mask_windDirection = (data_array[:, 10] >= valid_windDirection[0]) & (data_array[:, 10] <= valid_windDirection[1])
	mask_whiteListingWindDir = (data_array[:, 11] == valid_qualityInformation)
	mask_windSpeed = (data_array[:, 12] >= valid_windSpeed[0]) & (data_array[:, 12] <= valid_windSpeed[1])
	mask_whiteListingWindSpd = (data_array[:, 13] == valid_qualityInformation)
	mask_airTemperature = (data_array[:, 14] >= valid_airTemperature[0]) & (data_array[:, 14] <= valid_airTemperature[1])
	mask_whiteListingAirTemp = (data_array[:, 15] == valid_qualityInformation)
	mask_qualityInformation = (data_array[:, 16] == valid_qualityInformation)

	# Combine all masks
	combined_mask = mask_year & mask_month & mask_day & mask_hour & mask_minute & mask_second & \
		mask_latitude & mask_longitude & \
		mask_windSpeed & mask_whiteListingWindSpd & \
		mask_windDirection & mask_whiteListingWindDir & \
		mask_airTemperature & mask_whiteListingAirTemp & \
		mask_qualityInformation

	return combined_mask

def parse_box_heights(arg):
    return list(map(int, arg.strip().split(',')))

def lola2box(lon,lat,dlon_equator):
    boxj = np.deg2rad(lat) / dlon_equator / np.pi 
    dlon= dlon_equator/np.cos(np.deg2rad(lat))
    boxi = np.deg2rad(lon) / dlon / np.pi 

    return boxi,boxj

def write_emaddc_bufr(outfile,df_sel,thinned_string_data_array):
	fbufr = open(outfile, "wb")
	nodefR = CODES_MISSING_DOUBLE ; nodefI = CODES_MISSING_LONG

	template = ( 311010, 25061, 1015, 1022, 1065, 33002)
	associatedFields =['year','month', 'day','hour','minute','second', 'latitude', 'longitude', 'flightLevel',
		 'globalNavigationSatelliteSystemAltitude', 'detailedPhaseOfFlight',
		 'aircraftRollAngleQuality','aircraftTrueAirspeed','aircraftGroundSpeedUComponent',
		 'aircraftGroundSpeedVComponent',  'aircraftGroundSpeedWComponent', 'aircraftTrueHeading',
		 'aircraftHumiditySensors', 'mixingRatio','relativeHumidity','moistureQuality']

	max_subsets=100
	n_rows = len(df_sel)
	n_chunks = -(-n_rows // max_subsets)  # Ceiling division
	# Your DataFrame: df_sel
	for i in range(n_chunks):
		ibufr = codes_bufr_new_from_samples('BUFR4')
		start_idx = i * max_subsets
		end_idx = min((i + 1) * max_subsets, n_rows)
		df_chunk = df_sel.iloc[start_idx:end_idx]
		df_chunk.reset_index(drop=True, inplace=True)  # Reset index
		if df_chunk.empty:
			print("Skipping empty DataFrame")
			continue	
		# Update numberOfSubsets to match the actual number of records in the current chunk
		df_chunk_strings = thinned_string_data_array[start_idx:end_idx,:]
		actual_subsets = len(df_chunk)
		codes_set_array(ibufr, 'inputDelayedDescriptorReplicationFactor', (0, 0))
		codes_set_array(ibufr, 'inputShortDelayedDescriptorReplicationFactor', (0, 0, 0, 0, 0, 0))
		codes_set(ibufr, 'edition', 4)
		codes_set(ibufr, 'masterTableNumber', 0)
		codes_set(ibufr, 'bufrHeaderCentre', 99)
		codes_set(ibufr, 'bufrHeaderSubCentre', 99)
		codes_set(ibufr, 'updateSequenceNumber', 0)
		codes_set(ibufr, 'dataCategory', 4)
		codes_set(ibufr, 'internationalDataSubCategory', 2)
		codes_set(ibufr, 'dataSubCategory', 147)
		codes_set(ibufr, 'masterTablesVersionNumber', 33)
		codes_set(ibufr, 'localTablesVersionNumber', 0)
		codes_set(ibufr, 'typicalYear',   df_chunk['year'][0])
		codes_set(ibufr, 'typicalMonth',  df_chunk['month'][0])
		codes_set(ibufr, 'typicalDay',    df_chunk['day'][0])
		codes_set(ibufr, 'typicalHour',   df_chunk['hour'][0])
		codes_set(ibufr, 'typicalMinute', df_chunk['minute'][0])
		codes_set(ibufr, 'typicalSecond', df_chunk['second'][0])
		codes_set(ibufr, 'numberOfSubsets', actual_subsets)
		codes_set(ibufr, 'observedData', 0)
		codes_set(ibufr, 'compressedData', 1)
		codes_set_array(ibufr, 'unexpandedDescriptors', template)
		codes_set_array(ibufr, 'year',   df_chunk['year'].values)
		codes_set_array(ibufr, 'month',  df_chunk['month'].values)
		codes_set_array(ibufr, 'day',    df_chunk['day'].values)
		codes_set_array(ibufr, 'hour',   df_chunk['hour'].values)
		codes_set_array(ibufr, 'minute', df_chunk['minute'].values)
		codes_set_array(ibufr, 'second', df_chunk['second'].values)
		codes_set_array(ibufr, 'aircraftRegistrationNumberOrOtherIdentification', df_chunk_strings[:,0])
		# codes_set_array(ibufr, 'observationSequenceNumber', [0] * actual_subsets) is skipped
		codes_set_array(ibufr, 'latitude',  df_chunk['latitude'].values)
		codes_set_array(ibufr, 'longitude', df_chunk['longitude'].values)
		codes_set_array(ibufr, 'flightLevel', df_chunk['flightLevel'].values)
		codes_set_array(ibufr, 'detailedPhaseOfFlight', df_chunk['detailedPhaseOfFlight'].values)
		codes_set_array(ibufr, 'windDirection', df_chunk['windDirection'].values)
		codes_set_array(ibufr, 'windDirection->associatedField', df_chunk['windDirection->associatedField'].values)
		codes_set_array(ibufr, 'windSpeed', df_chunk['windSpeed'].values)
		codes_set_array(ibufr, 'windSpeed->associatedField', df_chunk['windSpeed->associatedField'].values)
		codes_set_array(ibufr, 'airTemperature', df_chunk['airTemperature'].values)
		codes_set_array(ibufr, 'airTemperature->associatedField', df_chunk['airTemperature->associatedField'].values)
		codes_set_array(ibufr, 'softwareVersionNumber', df_chunk_strings[:,1])
		codes_set_array(ibufr, 'featureName',  df_chunk_strings[:,2])
		codes_set_array(ibufr, 'icaoRegionIdentifier',  df_chunk_strings[:,3])
		codes_set(ibufr, 'aircraftRollAngleQuality', 0)
		codes_set_array(ibufr, 'stationOrSiteName', df_chunk_strings[:,4])
		codes_set(ibufr, 'qualityInformation', 0)
		codes_set(ibufr, 'windDirection->associatedField->associatedFieldSignificance', 8)
		codes_set(ibufr, 'windSpeed->associatedField->associatedFieldSignificance', 8)
		codes_set(ibufr, 'airTemperature->associatedField->associatedFieldSignificance', 8)
		for associatedField in associatedFields:
			 codes_set(ibufr, associatedField + '->associatedField', 3)
			 codes_set(ibufr, associatedField + '->associatedField->associatedFieldSignificance', 8)
		codes_set(ibufr, 'pack', 1)
		codes_write(ibufr, fbufr)

	print('Written to: ',outfile)
	fbufr.close()

if __name__ == "__main__":
	print(f"starting {sys.argv[0]} ")
	parser = argparse.ArgumentParser(description="Process BUFR data.")
	parser.add_argument("-f","--infile", type=str, help="Input BUFR file path.")
	parser.add_argument("--box_width", type=int, default=50, help="Box width in km.")
	parser.add_argument("--box_heights", type=parse_box_heights, default=[300,300,600,1000], help="Box heights in km, separated by commas.")
	parser.add_argument("--nMsgs", type=int, default=None, help="Number of Messages in file. Default read all messages")
	parser.add_argument("--DTG", type=int, default=2023083100, help="Date+HH for finding nearest times.")
	parser.add_argument("-o","--outfile", type=str, help="Output BUFR file path.")

	args = parser.parse_args()
	box_width = args.box_width  # in km
	# Calculate bi and bj
	n_messages = args.nMsgs  # Number of messages to read
	radius_earth = 6378  # in km
	box_width = args.box_width  # in km
	box_heights = args.box_heights  # in m, array
	dlon_equator = box_width / (2 * np.pi * radius_earth)
	try:
		infile = args.infile
		outfile = args.outfile
		data_array, string_data_array = read_emaddc_bufr(infile,n_messages)	
		print("unfiltered raw data:", data_array.shape)
	except Exception as e:
		print(e)
		print(__doc__)
		quit(0)

	# Combine all masks
	combined_mask = create_filter_mask(data_array)

	# Filter arrays using the combined mask
	filtered_data_array = data_array[combined_mask, :]
	filtered_string_data_array = string_data_array[combined_mask, :]

	print("filtered data:", filtered_data_array.shape)

	# Reference and epoch times
	ref_time = pd.to_datetime(args.DTG, format='%Y%m%d%H')
	print('Target_time',ref_time)
	epoch = pd.Timestamp('1970-01-01 00:00:00')


	# Convert to DataFrame for easier manipulation
	df = pd.DataFrame(filtered_data_array, columns=realkeys)
	df_string = pd.DataFrame(filtered_string_data_array, columns=stringkeys)
	for col in df_string.columns:
		df[col] = df_string[col]

	# Create datetime column
	df['datetime'] = pd.to_datetime(df[['year', 'month', 'day', 'hour', 'minute', 'second']])
	# Calculate box_time in seconds since ref_time
	# SdH: df['box_time'] = (df['datetime'] - ref_time).dt.total_seconds()
	df['dtime'] = (df['datetime'] - ref_time).dt.total_seconds().abs()

	# Calculate the total number of seconds since the epoch for the datetime column
	df['datetime_seconds'] = (df['datetime'] - epoch).dt.total_seconds()

	# Calculate dtime
	# SdH: df['dtime'] = np.abs(df['datetime_seconds'] - df['box_time'])

	df['bj'] = np.deg2rad(df['latitude']) / dlon_equator / np.pi
	dlon= dlon_equator/np.cos(np.deg2rad(df['latitude']))
	df['bi'] = np.deg2rad(df['longitude']) / dlon / np.pi
	df['bi'] = df['bi'].astype(int)
	df['bj'] = df['bj'].astype(int)

	box_height = [300, 300, 600, 1000]  # 300, 300, 600, 10000 lowest, 2nd lowest, ...
	height = [0]
	ih = 0

	while height[-1] < 50000:
		height.append(height[-1] + box_heights[ih])
		if height[-1] >= 300 and ih == 0:
			ih += 1
		elif height[-1] >= 600 and ih == 1:
			ih += 1
		elif height[-1] >= 1200 and ih == 2:
			ih += 1

	'''
	# harmonie 65 levels would be, e.g.:
	height = [
		31617.14, 24614.14, 21023.45, 18788.2, 17151.16, 15856.46, 14776.02,
		13838.65, 13010.38, 12273.46, 11611.35, 10999.73, 10421.76, 9872.665,
		9349.308, 8849.698, 8372.632, 7915.873, 7478.111, 7058.652, 6656.944,
		6272.61, 5904.423, 5551.39, 5213.203, 4889.508, 4579.747, 4283.422,
		4000.086, 3729.436, 3471.187, 3225.019, 2990.654, 2767.895, 2556.579,
		2356.544, 2167.637, 1989.728, 1822.765, 1666.755, 1521.701, 1387.292,
		1262.76, 1147.355, 1040.519, 941.6727, 850.2737, 765.8422, 687.9374,
		616.1398, 550.0368, 489.2245, 433.3172, 381.9429, 334.7357, 291.3341,
		251.3799, 214.5175, 180.3958, 148.6632, 118.9608, 90.9182, 64.1475,
		38.22901, 12.71386
	]
	'''

	df['bk'] = np.digitize(df['flightLevel'], height)

	# Sort DataFrame by datetime in descending order
	# SdH : df = df.sort_values('datetime', ascending=False).reset_index(drop=True)
	df = df.sort_values('dtime').reset_index(drop=True)

	# Group by boxes
	# SdH : df_sel = df.groupby(['bi', 'bj', 'bk'])
	# SdH : df_sel = df.loc[dfg['dtime'].idxmin()]
	# SdH : selected_indices = df_sel.index.values
	# SdH : df_sel = df_sel.reset_index(drop=True)
	df_sel = df.groupby(['bi', 'bj', 'bk']).first().reset_index(drop=True)
		
	# Convert back to NumPy array
	thinned_data_array = df_sel[realkeys].to_numpy()
	# SdH : thinned_string_data_array = filtered_string_data_array[selected_indices]
	thinned_string_data_array = df_sel[stringkeys].to_numpy()

	print(f"Thinned data from {len(df)} to {len(df_sel)}")

	# write df_sel to outfile
	write_emaddc_bufr(outfile,df_sel,thinned_string_data_array)
 
	print(f"finished {sys.argv[0]} ")